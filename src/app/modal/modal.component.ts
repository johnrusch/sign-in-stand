import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor() { }
  
  @ViewChild('DrawCanvas') iframe: ElementRef;

  @Input() attendee: any;
  @Input() closeModal: any;

  resetCanvas = () => {
    let doc = this.iframe.nativeElement.contentDocument || this.iframe.nativeElement.contentWindow;
    var event = new MouseEvent("dblclick");
    doc.dispatchEvent(event);
  }

  saveSignature = () => {
    
    let doc = this.iframe.nativeElement.contentDocument || this.iframe.nativeElement.contentWindow;
    let canvas = doc.getElementById("a");
    let image = canvas.toDataURL("image/jpeg").split(';base64,')[1];
    console.log(image);
    this.closeModal(this.attendee);

  }


  ngOnInit(): void {
  }

}
