import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sign-in-stand';

  attendees = [
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    },
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    },
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    },
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    },
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    },
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    },
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    },
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    },
    {
      name: "John Rusch"
    },
    {
      name: "Aiden Chelig"
    }
  ]
}
