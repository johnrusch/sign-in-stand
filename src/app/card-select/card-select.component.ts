import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-select',
  templateUrl: './card-select.component.html',
  styleUrls: ['./card-select.component.scss']
})
export class CardSelectComponent implements OnInit {

  constructor() { }

  @Input() attendees: any;
  public showModal: boolean = false;
  public selectedAttendee: any;

  cardSelect(attendee: any) {
    this.selectedAttendee = attendee;
    this.showModal = true;
  }

  closeModal = (response: any ) => {
    if (response === 'cancel') {
        this.selectedAttendee = null;
        this.showModal = false;
    } else if (response !== undefined) {
      this.selectedAttendee = null;
      this.showModal = false;
      this.attendees.find( (el: any) => {
        console.log(el.name);
        return el.name === response.name;
      }).checked = true;
    }
  }

  ngOnInit(): void {
  }

}
